import XCTest
@testable import TeyeAPI

final class TeyeAPITests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
      
      AuthAPI.obtainToken(grantType: "password", username: "john.doe@targetai.dev", password: "passW0rd", scope: "", clientId: "", clientSecret: "") { (tokenGet, error) in
        print(tokenGet)
        XCTAssertNotNil(error)
      }
//        XCTAssertEqual(TeyeAPI().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
