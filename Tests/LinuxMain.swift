import XCTest

import TeyeAPITests

var tests = [XCTestCaseEntry]()
tests += TeyeAPITests.allTests()
XCTMain(tests)
