//
// ONVIFCameraExposureUpdate.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ONVIFCameraExposureUpdate: Codable {

    public var value: Decimal

    public init(value: Decimal) {
        self.value = value
    }


}
