//
// LineCrossingAttributes.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct LineCrossingAttributes: Codable {

    public var objClass: LineCrossingEventObjectClass

    public init(objClass: LineCrossingEventObjectClass) {
        self.objClass = objClass
    }

    public enum CodingKeys: String, CodingKey { 
        case objClass = "obj_class"
    }

}
