//
// ONVIFCameraWideDynamicRangeUpdate.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ONVIFCameraWideDynamicRangeUpdate: Codable {

    public var mode: ONVIFWideDynamicRangeModes?
    public var value: Decimal?

    public init(mode: ONVIFWideDynamicRangeModes? = nil, value: Decimal? = nil) {
        self.mode = mode
        self.value = value
    }


}
