//
// FaceListGet.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct FaceListGet: Codable {

    public var name: String
    public var _description: String?
    public var priority: FaceListPriority?
    public var _id: Int
    public var faceCount: Int

    public init(name: String, _description: String? = nil, priority: FaceListPriority? = nil, _id: Int, faceCount: Int) {
        self.name = name
        self._description = _description
        self.priority = priority
        self._id = _id
        self.faceCount = faceCount
    }

    public enum CodingKeys: String, CodingKey { 
        case name
        case _description = "description"
        case priority
        case _id = "id"
        case faceCount = "face_count"
    }

}
