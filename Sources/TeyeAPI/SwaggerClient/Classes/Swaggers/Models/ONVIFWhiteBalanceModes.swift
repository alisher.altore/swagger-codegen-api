//
// ONVIFWhiteBalanceModes.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


/** An enumeration. */
public enum ONVIFWhiteBalanceModes: String, Codable {
    case auto = "AUTO"
    case manual = "MANUAL"
}